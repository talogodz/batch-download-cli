const utils = require('../utils')

test('split links', () => {
    const testLinks = " http://my.file.com/file,  ftp://other.file.com/other, sftp://and.also.this/ending etc";
    const expectResult = ["http://my.file.com/file", "ftp://other.file.com/other", "sftp://and.also.this/ending"]
    expect(utils.splitUrls(testLinks))
        .toEqual(expectResult);
});

test('split links from non-string type must return null', () => {
    expect(utils.splitUrls(123))
        .toEqual(null);
});

test('get filename from url without extension', () => {
    const testUrl = "http://my.file.com/file";
    expect(utils.getFilename(testUrl))
        .toEqual("file");
});

test('get filename from url with extension', () => {
    const testUrl = "ftp://my.file.com/file.txt";
    expect(utils.getFilename(testUrl))
        .toEqual("file.txt");
});

test('get filename from path without extension', () => {
    const testUrl = "/file";
    expect(utils.getFilename(testUrl))
        .toEqual("file");
});

test('get filename from path with extension', () => {
    const testUrl = "/dir/file.txt";
    expect(utils.getFilename(testUrl))
        .toEqual("file.txt");
});

test('get filename from non-string type must return null', () => {
    const testUrl = 123;
    expect(utils.getFilename(testUrl))
        .toEqual(null);
});

test('valid ftp url must be valid', () => {
    const testUrl = "ftp://my.file.com/file.txt";
    expect(utils.isUrlValid(testUrl))
        .toEqual(true);
});

test('invalid url must be invalid', () => {
    const testUrl = "some invalid url";
    expect(utils.isUrlValid(testUrl))
        .toEqual(false);
});


test('only-protocol url must be invalid', () => {
    const testUrl = "http://";
    expect(utils.isUrlValid(testUrl))
        .toEqual(false);
});

test('no-protocol url must be invalid', () => {
    const testUrl = "my.file.com/file.txt";
    expect(utils.isUrlValid(testUrl))
        .toEqual(false);
});

test('get url component from ftp', () => {
    const testUrl = "ftp://my.file.com/file.txt";
    const expectResult = {
        href: "ftp://my.file.com/file.txt",
        protocol: "ftp",
        hostname: "my.file.com",
        filename: "file.txt",
        path: "/file.txt",
        port: "",
    }

    expect(utils.getUrlComponent(testUrl))
        .toEqual(expectResult);
});

test('get url component from https with port', () => {
    const testUrl = "https://somedomain.com:80/image.png";
    const expectResult = {
        href: "https://somedomain.com:80/image.png",
        protocol: "http",
        hostname: "somedomain.com:80",
        filename: "image.png",
        path: "/image.png",
        port: "80",
    }

    expect(utils.getUrlComponent(testUrl))
        .toEqual(expectResult);
});


test('get url component from sftp', () => {
    const testUrl = "sftp://and.also.this/ending";
    const expectResult = {
        href: "sftp://and.also.this/ending",
        protocol: "sftp",
        hostname: "and.also.this",
        filename: "ending",
        path: "/ending",
        port: "",
    }

    expect(utils.getUrlComponent(testUrl))
        .toEqual(expectResult);
});



