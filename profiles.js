module.exports = {
    profiles: [
        {
            "protocol": "sftp",
            "hostname": "test.rebex.net",
            "username": "demo",
            "password": "password",
            "name": "rebex"
        },
        {
            "protocol": "ftp",
            "hostname": "test.rebex.net",
            "username": "demo",
            "password": "password",
            "name": "rebex"
        }
    ]
}