const inquirer = require('inquirer');
const mpd = require('multi-protocol-downloader');
const clear = require('clear');
const utils = require('./utils');
const { v4: uuidv4 } = require('uuid');
const profiles = require('./profiles').profiles;
const defaultDest = './downloads'

inquirer
    .prompt([{
        type: 'input',
        name: 'rawText',
        message: 'input links'
    },
    {
        type: 'input',
        name: 'dest',
        message: 'dest folder(./downloads)',
    }])
    .then(answers => {
        App(answers.rawText, answers.dest);
    });

const App = (rawText, dest) => {
    if (dest == '') dest = defaultDest;

    let fileObjects = utils.splitUrls(rawText)
        .map(e => attachProfileAndRef(profiles, e));

    const onUpdate = (status, ref, err) => {
        fileObjects = fileObjects.map(f => {
            if (f.ref == ref) f.status = status;
            return f;
        });
        draw(fileObjects);
        if (fileObjects.filter(f => ['finish', 'fail'].includes(f.status)).length == fileObjects.length) process.exit(0);
    }

    if (fileObjects.length) {
        draw(fileObjects);
        fileObjects.forEach(file => {
            mpd.download(
                file.href,
                {
                    dest,
                    savedName: file.savedName,
                    profile: file.profile,
                    ref: file.ref
                },
                onUpdate);
        });
    }
    else console.log('invalid input');
}

const draw = (fileObjects) => {
    clear();
    fileObjects.forEach(file => {
        console.log(`[${file.status}] (--${file.protocol}--) ${file.savedName}`);
    });
}

const attachProfileAndRef = (profileList, url) => {
    const parsedUrl = new URL(url);
    const [profile] = profileList.filter(
        p => p.hostname == parsedUrl.host && p.protocol == parsedUrl.protocol.replace(":", "")
    );
    const ref = uuidv4();
    const filenameFull = utils.getFilename(url);
    const [filename, ext] = filenameFull.split('.');
    let savedName = `${filename}-${ref}`;
    if (ext) savedName = `${savedName}.${ext}`;

    return {
        href: url,
        profile: profile || {},
        ref,
        status: 'pending',
        savedName,
        protocol: parsedUrl.protocol
    };
}
