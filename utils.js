const splitUrls = (urls) => {
    if (typeof (urls) != "string") return null;
    const regex = /(http|ftp|https|sftp):\/\/([\w_-]+(?:(?:\.[\w_-]+)+))([\w.@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?/g;
    return urls.match(regex);
}

const getFilename = (urlStr) => {
    if (typeof (urlStr) != "string") return null;
    return urlStr.substring(urlStr.lastIndexOf('/') + 1, urlStr.length);
}

const isUrlValid = (urlStr) => {
    try {
        new URL(urlStr);
        return true;
    } catch (_) {
        return false
    }
}

const getUrlComponent = (urlStr) => {
    if (typeof (urlStr) != "string") return null;

    const url = new URL(urlStr);

    let protocol = url.protocol.replace(":", "");
    if (protocol == "https") protocol = "http";

    return {
        href: url.href,
        protocol: protocol,
        hostname: url.host,
        filename: getFilename(url.pathname),
        path: url.pathname,
        port: url.port,
    }
}

const utils = {
    splitUrls,
    getFilename,
    getUrlComponent,
    isUrlValid,
}

module.exports = utils;